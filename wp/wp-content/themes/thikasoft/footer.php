<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

	</div><!-- #content -->

	</div><!-- #page -->

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/propper.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/wow/wow.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/sckroller/jquery.parallax-scroll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/isotope/isotope-min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/magnify-pop/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/counterup/appear.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/circle-progress/circle-progress.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/slick/slick.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/nice-select/jquery.nice-select.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/css/vendors/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/skip-link-focus-fix.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/customize-controls.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/customize-preview.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/priority-menu.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/touch-keyboard-navigation.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/canvas.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/comming-soon.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/mchimpsubs.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/parallax.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/typed.min.js"></script>
    

    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB13ZAvCezMx5TETYIiGlzVIq65Mc2FG5g"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/gmaps.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>

    <!-- contact js -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.form.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/contact.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>


<?php wp_footer(); ?>

</body>
</html>
