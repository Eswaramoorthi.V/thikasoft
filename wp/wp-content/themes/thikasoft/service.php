<?php
/* Template Name: Service Page */

get_header();
?>

<style type="text/css">
    .payment_footer_area_two{padding-top: 250px;}
</style>
    <?php include('components/pre-loading.php'); ?>
    
    <div class="body_wrapper">
        
        <?php include('components/header-menu.php'); ?>

        <section class="breadcrumb_area">
            <img class="breadcrumb_shap" src="<?php echo get_template_directory_uri(); ?>/img/breadcrumb/banner_bg.png" alt="">
            <div class="container">
                <div class="breadcrumb_content text-center">
                    <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Our services</h1>
                    <p class="f_400 w_color f_size_16 l_height26">Why I say old chap that is spiffing off his nut arse pear shaped plastered<br> Jeffrey bodge barney some dodgy.!!</p>
                </div>
            </div>
        </section>
        <section class="prototype_service_info">
            <div class="symbols-pulse active">
                <div class="pulse-1"></div>
                <div class="pulse-2"></div>
                <div class="pulse-3"></div>
                <div class="pulse-4"></div>
                <div class="pulse-x"></div>
            </div>
            <div class="container">
                <h2 class="f_size_30 f_600 t_color3 l_height45 text-center mb_90">SaasLand is built for designers like you.<br> With useful features, an intuitive interface.</h2>
                <div class="row p_service_info">
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pr_70">
                            <div class="icon icon_one"><i class="ti-panel"></i></div>
                            <h5 class="f_600 f_p t_color3">Export Presets</h5>
                            <p class="f_400">Bog cheeky bugger blow off only a quid grub he legged it porkies tosser young delinquent argy-bargy.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_50 pr_20">
                            <div class="icon icon_two"><i class="ti-layout-grid2"></i></div>
                            <h5 class="f_600 f_p t_color3">Grid and Guides</h5>
                            <p class="f_400">Bog cheeky bugger blow off only a quid grub he legged it porkies tosser young delinquent argy-bargy.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_70">
                            <div class="icon icon_three"><i class="ti-fullscreen"></i></div>
                            <h5 class="f_600 f_p t_color3">Pixel Precision</h5>
                            <p class="f_400">Bog cheeky bugger blow off only a quid grub he legged it porkies tosser young delinquent argy-bargy.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pr_70">
                            <div class="icon icon_four"><i class="ti-vector"></i></div>
                            <h5 class="f_600 f_p t_color3">Vector Editing</h5>
                            <p class="f_400">Bog cheeky bugger blow off only a quid grub he legged it porkies tosser young delinquent argy-bargy.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_50 pr_20">
                            <div class="icon icon_five"><i class="ti-cloud-down"></i></div>
                            <h5 class="f_600 f_p t_color3">Cloud Service</h5>
                            <p class="f_400">Bog cheeky bugger blow off only a quid grub he legged it porkies tosser young delinquent argy-bargy.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="p_service_item pl_70">
                            <div class="icon icon_six"><i class="ti-bolt"></i></div>
                            <h5 class="f_600 f_p t_color3">Iterate at Speed</h5>
                            <p class="f_400">Bog cheeky bugger blow off only a quid grub he legged it porkies tosser young delinquent argy-bargy.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="prototype_service_area_two">
            <div class="container custom_container">
                <div class="sec_title text-center mb_70">
                    <h2 class="f_p f_size_30 l_height50 f_600 t_color3">Your enterprise carrier in the Cloud</h2>
                    <p class="f_400 f_size_16 mb-0">WShow off show off pick your nose and blow off Elizabeth grub haggle <br> dropped a clanger cracking.!</p>
                </div>
                <div class="service_carousel owl-carousel">
                    <div class="service_item">
                        <div class="icon s_icon_one"><i class="ti-check"></i></div>
                        <h4 class="f_600 f_size_20 l_height28 t_color2 mb_20">Mail Metrics<br> Receive</h4>
                        <p>Brolly off his nut A bit of how's your father chancer in my flat chinwag bog skive.</p>
                        <img class="float-right" src="<?php echo get_template_directory_uri(); ?>/img/new/undraw.png" alt="">
                    </div>
                    <div class="service_item">
                        <div class="icon s_icon_two"><i class="ti-location-arrow"></i></div>
                        <h4 class="f_600 f_size_20 l_height28 t_color2 mb_20">Mail Metrics<br> Receive</h4>
                        <p>Brolly off his nut A bit of how's your father chancer in my flat chinwag bog skive.</p>
                        <img class="float-right" src="<?php echo get_template_directory_uri(); ?>/img/new/inbox.png" alt="">
                    </div>
                    <div class="service_item">
                        <div class="icon s_icon_three"><i class="ti-search"></i></div>
                        <h4 class="f_600 f_size_20 l_height28 t_color2 mb_20">Mail Metrics<br> Receive</h4>
                        <p>Brolly off his nut A bit of how's your father chancer in my flat chinwag bog skive.</p>
                        <img class="float-right" src="<?php echo get_template_directory_uri(); ?>/img/new/file.png" alt="">
                    </div>
                    <div class="service_item">
                        <div class="icon s_icon_four"><i class="ti-stats-up"></i></div>
                        <h4 class="f_600 f_size_20 l_height28 t_color2 mb_20">Mail Metrics<br> Receive</h4>
                        <p>Brolly off his nut A bit of how's your father chancer in my flat chinwag bog skive.</p>
                        <img class="float-right" src="<?php echo get_template_directory_uri(); ?>/img/new/report.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="partner_logo_area_two">
            <div class="container">
                <h4 class="f_size_18 f_400 f_p text-center l_height28 mb_70">Market leaders use app to nrich their brand & business.</h4>
                <div class="row partner_info">
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_01.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_02.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_03.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_04.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_05.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_03.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_04.png" alt=""></a>
                    </div>
                    <div class="logo_item">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_05.png" alt=""></a>
                    </div>
                </div>

                <div class="subscribe_form_info s_form_info_two text-center">
                    <h2 class="f_600 f_size_30 l_height30 t_color3 mb_50">Subscribe for ger exclusive news & offer</h2>
                    <form action="#" class="subscribe-form">
                        <input type="text" class="form-control" placeholder="Your email">
                        <button type="submit" class="btn_hover btn_four mt_40">Subscribe</button>
                    </form>
                </div>
            </div>
        </section>

        <?php include('components/footer-menu.php'); ?>
        
    </div>

    <?php
get_footer();
