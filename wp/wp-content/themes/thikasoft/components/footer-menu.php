<footer class="payment_footer_area payment_footer_area_two">
            <div class="footer_top_six">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="f_widget company_widget">
                                <a href="<?php echo get_site_url(); ?>" class="f-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/main-logo.png" alt="logo"></a>
                                <p class="mt_40">Copyright © 2021</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="f_widget about-widget">
                                <h3 class="f-title f_600 w_color f_size_18 mb_40">Quick Link</h3>
                                <ul class="list-unstyled f_list">
                                    <?php
                  wp_nav_menu( array(
                    'theme_location' => 'my-custom-menu-footer',
                    'container_class' => 'custom-menu-class' ) );
                    ?>
                                    <!-- <li><a href="#">Company</a></li>
                                    <li><a href="#">Android App</a></li>
                                    <li><a href="#">ios App</a></li>
                                    <li><a href="#">Desktop</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="f_widget about-widget">
                                <h3 class="f-title f_600 w_color f_size_18 mb_40">Help?</h3>
                                <ul class="list-unstyled f_list">
                                    <li><a href="<?php the_permalink(); ?>faq">FAQ</a></li>
                                    <!-- <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Term & conditions</a></li>
                                    <li><a href="#">Reporting</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-6">
                            <div class="f_widget social-widget">
                                <h3 class="f-title f_600 w_color f_size_18 mb_40">Follow Us</h3>
                                <div class="f_social_icon">
                                    <a href="#" class="ti-facebook"></a>
                                    <a href="#" class="ti-twitter-alt"></a>
                                    <a href="#" class="ti-vimeo-alt"></a>
                                    <a href="#" class="ti-pinterest"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>